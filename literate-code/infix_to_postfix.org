#+TITLE: Infix to Postfix Conversion and Evaluation of Postfix 
#+AUTHOR: VLEAD
#+DATE: [2018-06-13]
#+SETUPFILE: ./../org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


* Introduction
- This document contains js for Conversiin of Infix to Postfix Expression Artifact.
- various steps of Conversion are written here like :-
  - Scan element by element and push to stack or type it to output.
  - If ')' comes then pop it to output or to ignore.
  - Check answer and Restart are there for the purpose as said by the name.
- Contains an API for Conversion of Infix to POstfix Artefact.
- Div elements of html are appended to the body using javascript
  append child tag. 
- This is the org file for Insert exercise which will
  be tangled as infix_to_postfix.js.




* Create DOM elements for the HTML
#+NAME: Create Elements
#+BEGIN_SRC javascript

            var asddiv = document.createElement("div");
            asddiv.id = "infix_to_postfix";
            divElem.appendChild(asddiv);
            var arr = [],temp;
            var element_generate = new Elements();
            console.log("New");
            var infix = sessionStorage.getItem('infix').split(',');
            temp = element_generate.create_element("text", "question");
            temp.innerHTML="Convert the following expression to postfix: " + infix.join(' ') ;
            arr.push(temp);
            arr.push(element_generate.create_element("input", "pushtostack"));
            arr.push(element_generate.create_button("push_tostack_button", "btn btn-danger", "", "button", "Push To Stack"));
            arr.push(element_generate.create_element("input", "typetooutput"));
            arr.push(element_generate.create_button("type_tooutput_button", "btn btn-danger", "", "button", "Type To Output"));
            arr.push(element_generate.create_button("pop_button", "btn btn-danger", "", "button", "Pop"));
            arr.push(element_generate.create_radio_button("input", "tooutput", "radio_button", "", "To Output"));
            temp = element_generate.create_element("text","");
            temp.innerHTML="To Output"
            arr.push(temp);
            arr.push(element_generate.create_radio_button("input", "toignore", "radio_button", "", "To Ignore"));
            temp = element_generate.create_element("text","");
            temp.innerHTML="To Ignore"
            arr.push(temp);
            var canvas_test = element_generate.create_element("canvas", "canvas_for_conversion");
            canvas_test.width = "500";
	    canvas_test.height = "600";
            canvas_test.innerHTML = "Your browser does not support canvas.";
            canvas_test.style = "margin-top:100px;";
            arr.push(canvas_test);
            arr.push(element_generate.create_element("text", "output"));
            arr.push(element_generate.create_element("text", "result"));
            arr.push(element_generate.create_button("restart_button", "btn btn-info", "", "reset", "Restart"));
            arr.push(element_generate.create_button("check_answer", "btn btn-success", "", "button", "Check Answer"));
            $('#infix_to_postfix').append('<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/4.4.2/math.js"></script>');
            $('#infix_to_postfix').append('<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/4.4.2/math.min.js"></script>');
            $("#infix_to_postfix").append(arr);



#+END_SRC


* Global Variables
#+NAME: globals
#+BEGIN_SRC javascript

            var canvas = document.getElementById("canvas_for_conversion");
            var ctx = canvas.getContext("2d");
            var values1 = new Array();
            var values2 = new Array();
            var pointer = 0;
            var exp_start_x = 100;
            var exp_start_y = 100;
            var box_start_x = 300;
            var box_start_y = 600;
            var box_height = 40;
            var box_length = 150;

#+END_SRC


* Clear the Canvas
#+NAME: canvas_clear
#+BEGIN_SRC javascript

            function clearCanvas() {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
            }

#+END_SRC


* Draw the stack element box
#+NAME: Draw_box
#+BEGIN_SRC javascript

            function draw_box() {


                clearCanvas();
                ctx.beginPath();
                for (var i = 0; i < values1.length; i++) {

                    ctx.fillStyle = "blue";
                    ctx.fillRect(box_start_x, box_start_y - box_height * i, box_length, box_height);
                    ctx.stroke();
                    ctx.strokeStyle = "white";
                    ctx.rect(box_start_x, box_start_y - box_height * i, box_length, box_height);
                    ctx.stroke();
                }
                ctx.closePath();


            }

#+END_SRC


* Draw the Numbers
#+NAME: draw_numbers
#+BEGIN_SRC javascript

                function writeNumbers() {


                //draw_expression();
                draw_box();
                // draw_expression();
                //Fill array with numbers
                // console.log(values);
                for (var i = 0; i < values1.length; i++) {

                    ctx.font = "25px Arial";
                    // ctx.font = font_size;
                    // var txt = values[i].join(' ');
                    var txt = values1[i].toString();
                    var txtWidth = ctx.measureText(txt).width;

                    //Coordinates of numbers

                    var txtX = box_start_x + box_length / 2 - txtWidth / 2;
                    var txtY = box_start_y - box_height * i + (box_height) / 2 + 8;
                    ctx.fillStyle = "white";
                    ctx.fillText(txt, txtX, txtY);

                }

            }

#+END_SRC


* On-click Function for Push to Stack Button
#+NAME: Onclick_Push_to_Stack
#+BEGIN_SRC javascript

            $('#push_tostack_button').on('click', function () {
                // values1.push(infix[pointer]);
                values1.push(document.getElementById('pushtostack').value)
                pointer++;
                writeNumbers();
            });


#+END_SRC


* On-click Function to Push to Output String
#+NAME: Onclick_Push_to_Output_string
#+BEGIN_SRC javascript

            $('#type_tooutput_button').on('click', function () {
                // values2.push(infix[pointer]);
                values2.push(document.getElementById('typetooutput').value);
                // pointer++;
                document.getElementById('output').innerHTML="Output: " + values2.join(' ');
                writeNumbers();
            });


#+END_SRC


* On-click of Pop Button
#+NAME: Pop_button
#+BEGIN_SRC javascript

            $('#pop_button').on('click', function () {
                popped_elements = new Array();
                popped_elements.push(values1.pop());
                writeNumbers();
            });

#+END_SRC


* To Output of the Popped Element
#+NAME: To_Output
#+BEGIN_SRC javascript

            $('#tooutput').on('click',function (){
                values2.push(popped_elements);
                popped_elements = new Array();
                document.getElementById('output').innerHTML="Output: " + values2.join(' ');
                writeNumbers();
                console.log("test");
                

            });

#+END_SRC


* To Remove the Popped Element
#+NAME: To_remove
#+BEGIN_SRC javascript

            $('#toignore').on('click',function (){
                popped_elements = new Array();
                writeNumbers();

            });

#+END_SRC


* To Restart the Exercise
#+NAME: To_restart
#+BEGIN_SRC javascript

            $('#restart_button').on('click', function () {
                values1 = new Array();
                values2 = new Array();
                pointer = 0;
                document.getElementById('output').innerHTML="";
                document.getElementById('result').innerHTML = "";
                clearCanvas();
            });


#+END_SRC


* To check the answer
#+NAME: Check_answer
#+BEGIN_SRC javascript

            $('#check_answer').on('click', function () {
                var infix_instance = new Postfix_check();
                //console.log(infix);
                var postfix = infix_instance.infix_to_postfix(infix).join('');console.log(postfix);
                
                var values_str = values2.join('');
                if (postfix != values_str) {
                    //console.log(postfix);
                    //console.log(values_str)
                    document.getElementById('result').innerHTML = "Wrong";

                } else {
                    document.getElementById('result').innerHTML = "Correct";
                }
            });


#+END_SRC


* Creator function Tangle
#+NAME: Creator
#+BEGIN_SRC javascript :tangle js/code-4.js :eval no :noweb yes

var PopandPushoperations = function () {

    var obj = {};
    var infix_to_postfix_conversion = function (divElem) {
  

           $(document).ready(function () {
             <<Create Elements>>
<<globals>>
<<canvas_clear>>
<<Draw_box>>
<<draw_numbers>>
<<Onclick_Push_to_Stack>>
<<Onclick_Push_to_Output_string>>
<<Pop_button>>
<<To_Output>>
<<To_remove>>
<<To_restart>>
<<Check_answer>>


        });


  
    };

    obj.infix_to_postfix_conversion = infix_to_postfix_conversion;
    return obj;
};

#+END_SRC
